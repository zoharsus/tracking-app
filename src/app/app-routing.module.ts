import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderListComponent } from './orders/order-list/order-list.component';
import { OrderViewComponent } from './orders/order-view/order-view.component';
import { OrderCreateComponent } from './orders/order-create/order-create.component';

import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', component: OrderListComponent },
  { path: 'view/:orderTracking', component: OrderViewComponent },
  { path: 'create', component: OrderCreateComponent, canActivate: [AuthGuard] },
  { path: 'edit/:orderId', component: OrderCreateComponent, canActivate: [AuthGuard] },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
